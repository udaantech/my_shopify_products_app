namespace :sample do
  desc 'Delete the zip files'
  task :test => [ :environment ] do
    @shops = Shop.all
    @shops.each do |shop|
      FileUtils.rm_rf("#{Rails.root}/tmp/#{shop.id}/zips") 
      puts "removed"
      Dir.mkdir("#{Rails.root}/tmp/#{shop.id}/zips") unless Dir.exist?("#{Rails.root}/tmp/#{shop.id}/zips")
      puts "created"
    end
  end
end