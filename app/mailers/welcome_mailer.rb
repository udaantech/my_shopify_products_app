class WelcomeMailer < ApplicationMailer
  def welcome_email(uemail)
    mail(to: uemail, subject: 'Sync details')
  end
end
