class ApplicationMailer < ActionMailer::Base
  default from: 'admin@happifyapps.com'
  layout 'mailer'
end
