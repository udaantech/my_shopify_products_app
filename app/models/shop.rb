# frozen_string_literal: true
class Shop < ActiveRecord::Base
  include ShopifyApp::ShopSessionStorage
  has_many :products,dependent: :destroy
  has_one :sync_status,dependent: :destroy
  def api_version
    ShopifyApp.configuration.api_version
  end

  after_create do
    sync_status = self.build_sync_status
    sync_status.save
    Dir.mkdir("#{Rails.root}/tmp/#{self.id}") unless Dir.exist?("#{Rails.root}/tmp/#{self.id}")
    Dir.mkdir("#{Rails.root}/tmp/#{self.id}/zips") unless Dir.exist?("#{Rails.root}/tmp/#{self.id}/zips")
  end

  before_update do
    if self.shopify_token_changed?
      self.app_uninstalled = false
      self.charge_cancelled = false
    end
  end
end