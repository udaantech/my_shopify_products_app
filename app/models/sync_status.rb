class SyncStatus < ApplicationRecord
  belongs_to :shop
  enum auto_sync: { not_started: 1, in_progress: 2,completed: 3 }
end
