class ProductCreateWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3
  sidekiq_options queue: 'default'

  def perform(params = {})
    shop = Shop.find_by(shopify_domain: params["shop_domain"])
    return unless shop
    ProductSyncWorker.perform_async(shop.id,params["webhook"]['id'])
  end
end