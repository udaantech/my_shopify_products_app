class ProductDeleteWorker
  include Sidekiq::Worker
  sidekiq_options retry: false
  sidekiq_options queue: 'default'

  def perform(params = {})
    shop = Shop.find_by(shopify_domain: params["shop_domain"])
    return unless shop
    product = shop.products.find(params["webhook"]['id'])
    product.destroy if product
  end
end
