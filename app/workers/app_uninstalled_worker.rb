class AppUninstalledWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3
  sidekiq_options queue: 'default'

  def perform(params = {})
    shop = Shop.find_by(shopify_domain: params["shop_domain"])
    return unless shop
    shop.sync_status.update(auto_sync: 1)
    shop.update(app_uninstalled: true)
    # shop.update(charge_cancelled: true)
  end
end