class ProductSyncWorker
  include Sidekiq::Worker
  sidekiq_options retry: 5
  sidekiq_options queue: 'default'

  require 'rqrcode_png'

  def perform(shop_id,product_id = nil)
    @shop = Shop.find_by(id: shop_id)
    return unless @shop
    @shop.with_shopify_session do
      begin
        shop_data = ShopifyAPI::Shop.current
        json_shop = convert_to_json(shop_data)
        @shop.update(email: json_shop['email'])
        unless product_id.present?
          products = ShopifyAPI::Product.find(:all, params: { limit: 250 })
          while products
            save_products(products)
            products = products.next_page? ? products.fetch_next_page : nil
          end
          @shop.sync_status.update(auto_sync: 3)
        else
          products = [ShopifyAPI::Product.find(product_id)]
          save_products(products)
        end
      rescue StandardError => e
          puts "#{e}"
      end
    end
    WelcomeMailer.welcome_email(@shop.email).deliver_now
    # uemail = "sumit459goyal@gmail.com"
    # WelcomeMailer.welcome_email(uemail).deliver_now
  end

  private
  
  def save_products(products)
    products.each do |product|
      json_product = convert_to_json(product)
      db_product = @shop.products.find_or_initialize_by(id: json_product['id'])
      db_product.shopify_product = json_product
      @qr = RQRCode::QRCode.new("https://#{@shop.shopify_domain}/products/#{json_product["handle"]}").to_img.resize(100, 100).to_data_url
      db_product.qr_code = @qr
      db_product.save
    end
  end

  def convert_to_json(data)
    json_c = data.to_json
    JSON.parse(json_c)
  end

end
