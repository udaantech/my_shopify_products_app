class MandatoryWebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token

  def shop_redact
    params.permit!
    render json: {message: 'ok'},status: :ok
  end

  def customer_redact
    params.permit!
    render json: {message: 'ok'},status: :ok
  end

  def customer_data_request
    params.permit!
    render json: {message: 'ok'},status: :ok
  end
  
  private
  #Get all Parameter except controller,action and type.
  def webhook_params
    params.except(:controller, :action, :type)
  end

end