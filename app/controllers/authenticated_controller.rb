# frozen_string_literal: true

class AuthenticatedController < ApplicationController
  include ShopifyApp::Authenticated

  before_action :current_shop

  def current_shop
    return unless session[:shopify_domain]
    @current_shop ||= Shop.find_by_shopify_domain(session[:shopify_domain])
  end
end
