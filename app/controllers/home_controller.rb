class HomeController < AuthenticatedController
require 'zip'

  def index
    @status = get_status
    if !params[:params1].nil? && params[:params1] == "false" && !@current_shop.status
      @message = "false"
    end
    if @current_shop.sync_status.auto_sync == "completed"
     x = 1
    else
      redirect_to syncing_path
    end
  end

 def update_status
  a = params[:myparams1]
  if a == "false"
    @current_shop.update(status: false)
  else
    @current_shop.update(status: true)
  end
 end

 def get_status
  if @current_shop.status == true
    @status = "checked"
  else
    @status = ""
  end
  @status
 end
  
  def auto_sync
    @status = get_status
    if !params[:params2].nil? && params[:params2] == "false"
      @message = "false"
    end
    if @current_shop.sync_status.auto_sync == "not_started"
      @current_shop.sync_status.update(auto_sync: 2)
      ProductSyncWorker.perform_async(@current_shop.id)
    elsif @current_shop.sync_status.auto_sync == "completed"
      redirect_to home_path
    end
  end

  def sync_status
    render json: {sync_status: @current_shop.sync_status.auto_sync}
  end

  def bulk_download
    if @current_shop.sync_status.auto_sync == "completed"
      if @current_shop.status == true
        a = []
        product_ids = params[:ids].nil? ? a.push(params[:id]) : params[:ids]
        @products = @current_shop.products.where(:id => product_ids)
        # randoms = rand()
        filename = "qr_"+Time.now.to_s.split(" ")[0]+"_"+Time.now.to_s.split(" ")[1]+".zip"
         folder1 = "#{Rails.root}/tmp/#{@current_shop.id}"
         folder = "#{Rails.root}/tmp/#{@current_shop.id}/zips"
         temp_file = "#{folder}/#{filename}"
        begin
          Zip::OutputStream.open(temp_file) { |zos| }
    
          Zip::File.open(temp_file, Zip::File::CREATE) do |zipfile|
            @products.each do |product|
              content = product.qr_code.split("base64,")[1]
              File.open("#{folder1}/#{product.id}.png", "wb") do |f|
                f.write(Base64.decode64(content))
              end 
              zipfile.add("#{product.id}.png", "#{folder1}/#{product.id}.png")
            end
          end
          send_file(temp_file, type: 'application/zip', filename: filename)
        ensure 
        end
      else
        redirect_to home_path(params1: "false")
      end
    else
      redirect_to syncing_path(params2: "false")
    end
  end
end

