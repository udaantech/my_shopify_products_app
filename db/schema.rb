# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_08_17_104703) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "products", id: :bigint, default: nil, force: :cascade do |t|
    t.jsonb "shopify_product", default: {}, null: false
    t.bigint "shop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "qr_code"
    t.index ["shop_id"], name: "index_products_on_shop_id"
    t.index ["shopify_product"], name: "index_products_on_shopify_product", using: :gin
  end

  create_table "shops", force: :cascade do |t|
    t.string "shopify_domain", null: false
    t.string "shopify_token", null: false
    t.bigint "charge_id"
    t.boolean "charge_cancelled", default: false
    t.datetime "trial_ends_on"
    t.datetime "billing_on"
    t.boolean "app_uninstalled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.string "subscription"
    t.boolean "status", default: true
    t.index ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true
  end

  create_table "sync_statuses", force: :cascade do |t|
    t.integer "auto_sync", default: 1
    t.bigint "shop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shop_id"], name: "index_sync_statuses_on_shop_id"
  end

  add_foreign_key "products", "shops"
  add_foreign_key "sync_statuses", "shops"
end
