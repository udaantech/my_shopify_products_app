class AddSubscriptionToShops < ActiveRecord::Migration[5.2]
  def change
    add_column :shops, :subscription, :string
  end
end
