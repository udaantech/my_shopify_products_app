class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products, id: :bigint, default: nil  do |t|
      t.jsonb :shopify_product,null: false,default: {}
      t.references :shop, foreign_key: true

      t.timestamps
    end
    add_index :products, :shopify_product, using: 'gin'
  end
end
