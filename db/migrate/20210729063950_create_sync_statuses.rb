class CreateSyncStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :sync_statuses do |t|
      t.integer :auto_sync,default: 1
      t.references :shop, foreign_key: true

      t.timestamps
    end
  end
end
