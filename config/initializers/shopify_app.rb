ShopifyApp.configure do |config|
  config.application_name = "My Shopify App"
  config.old_secret = ""
  config.api_key = Rails.application.credentials[Rails.env.to_sym][:shopify_api_key]
  config.secret = Rails.application.credentials[Rails.env.to_sym][:shopify_api_secret]
  config.scope = "read_products, read_orders, write_orders, write_products" 
  config.embedded_app = false
  config.after_authenticate_job = false
  config.api_version = "2021-07"
  config.shop_session_repository = 'Shop'

  # config.reauth_on_access_scope_changes = true

  config.allow_jwt_authentication = false
  config.allow_cookie_authentication = true

  # config.api_key = ENV.fetch('SHOPIFY_API_KEY', '').presence
  # config.secret = ENV.fetch('SHOPIFY_API_SECRET', '').presence
  # if defined? Rails::Server
  #   raise('Missing SHOPIFY_API_KEY. See https://github.com/Shopify/shopify_app#requirements') unless config.api_key
  #   raise('Missing SHOPIFY_API_SECRET. See https://github.com/Shopify/shopify_app#requirements') unless config.secret
  # end

  config.webhooks = [
    {topic: 'products/update', address: "#{Rails.application.credentials[Rails.env.to_sym][:base_app_url]}/webhooks/product_create", format: 'json'},
    {topic: 'products/create', address: "#{Rails.application.credentials[Rails.env.to_sym][:base_app_url]}/webhooks/product_create", format: 'json'},
    {topic: 'app/uninstalled', address: "#{Rails.application.credentials[Rails.env.to_sym][:base_app_url]}/webhooks/app_uninstalled", format: 'json'},
    {topic: 'products/delete', address: "#{Rails.application.credentials[Rails.env.to_sym][:base_app_url]}/webhooks/product_delete", format: 'json'}
  ]

end

