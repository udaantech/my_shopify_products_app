Rails.application.routes.draw do
  require 'sidekiq/web'
  # root :to => 'recurring#index'
  root :to => 'home#index'
  # get '/index',to: 'recurring#index'
  # get '/charge',to: 'recurring#create_recurring_application_charge'
  mount ShopifyApp::Engine, at: '/'
  get '/home',to: 'home#index'
  post '/update_status',to: 'home#update_status'
  # get '/products',to: 'home#products'
  get '/download',to: 'home#bulk_download'
  # get '/products', :to => 'products#index'
  get 'syncing',to: 'home#auto_sync'
  get 'sync_status',to: 'home#sync_status'
  # get 'create_charge',to: 'recurring#create_recurring_application_charge'
  # get 'activatecharge',to: 'recurring#activate_charge'
  # post '/cancel/charge',to: 'recurring#cancel_charge'


    #mandatory webhooks endpoints
    post 'shop_redact',to: 'mandatory_webhooks#shop_redact', defaults: {format: "json"}
    post 'customers_redact',to: 'mandatory_webhooks#customer_redact', defaults: {format: "json"}
    post 'customers_data_request',to: 'mandatory_webhooks#customer_data_request', defaults: {format: "json"}
    
end
